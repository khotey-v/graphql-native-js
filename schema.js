import graphql, {
  GraphQLObjectType,
  GraphQLInt,
  GraphQLString,
  GraphQLSchema,
  GraphQLList,
  GraphQLNonNull,
} from 'graphql'
import Db, {
  Person,
  Post,
} from './db'

const PersonSchema = new GraphQLObjectType({
  name: 'Person',
  description: 'This represents a person',
  fields: () => ({
    id: {
      type: GraphQLInt,
      resolve(person) {
        return person.id
      }
    },

    firstName: {
      type: GraphQLString,
      resolve(person) {
        return person.firstName
      }
    },

    lastName: {
      type: GraphQLString,
      resolve(person) {
        return person.lastName
      }
    },

    email: {
      type: GraphQLString,
      resolve(person) {
        return person.email
      }
    },

    posts: {
      type: new GraphQLList(PostSchema),
      resolve(person, args) {
        return person.getPosts();
      }
    }
  })
})

const PostSchema = new GraphQLObjectType({
  name: 'Post',
  description: 'This represents of post',
  fields: () => ({
    id: {
      type: GraphQLInt,
      resolve(post) {
        return post.id
      }
    },

    title: {
      type: GraphQLString,
      resolve(post) {
        return post.title
      }
    },

    content: {
      type: GraphQLString,
      resolve(post) {
        return post.content
      }
    },

    author: {
      type: PersonSchema,
      resolve(post, args) {
        return post.getPerson();
      }
    }
  })
})

const Query = new GraphQLObjectType({
  name: 'Query',
  description: 'This is root quey, start endpoint ',
  fields: () => ({
    people: {
      type: new GraphQLList(PersonSchema),
      // limit possible arguments
      args: {
        id: {
          type: GraphQLInt
        },
        email: {
          type: GraphQLString,
        }
      },
      resolve(root, args) {
        return Person.findAll({ where: args })
      }
    },

    posts: {
      type: new GraphQLList(PostSchema),
      resolve(root, args) {
        return Post.findAll({ where: args })
      }
    }
  })
})

const Mutation = new GraphQLObjectType({
  name: 'Mutation',
  description: 'Functions to create stuff',
  fields: () => ({
    addPerson: {
      type: PersonSchema,
      args: {
        firstName: {
          type: new GraphQLNonNull(GraphQLString),
        },
        lastName: {
          type: new GraphQLNonNull(GraphQLString),
        },
        email: {
          type: new GraphQLNonNull(GraphQLString),
        }
      },
      resolve(_, { firstName, lastName, email}) {
        return Person.create({
          firstName,
          lastName,
          email: email.toLowerCase(),
        });
      }
    }
  })
})

const Schema = new GraphQLSchema({
  query: Query,
  mutation: Mutation,
})

export default Schema