import express from 'express'
import GraphHTTP from 'express-graphql'
import Schema from './schema'

 const APP_PORT = process.env.PORT || 3000
 const app = express()

 app.use('/graphql', GraphHTTP({
  schema: Schema,
  pretty: true, // disable minify response text
  graphiql: true,
 }))

 app.listen(APP_PORT, err => {
  if (err) {
    console.log(err)
  } else {
    console.log(`GraphQl server running on ${APP_PORT} port`)
  }
 })