import Sequelize from 'sequelize'
import _ from 'lodash'
import Faker from 'faker'

const Conn = new Sequelize('test', null, null, {
  dialect: 'sqlite',
  storage: './test_db.sqlite'
})

const Person = Conn.define('person', {
  firstName: { type: Sequelize.STRING, allowNull: false },
  lastName: { type: Sequelize.STRING, allowNull: false },
  email: { type: Sequelize.STRING, allowNull: false, validate: {
    isEmail: true
  } },
})

const Post = Conn.define('post', {
  title: { type: Sequelize.STRING, allowNull: false },
  content: { type: Sequelize.STRING, allowNull: false },
})

Person.hasMany(Post)
Post.belongsTo(Person)

// default data
Conn
  .sync({ force: true })
  .then(() => {
    _.times(10, () => {
      return Person.create({
        firstName: Faker.name.firstName(),
        lastName: Faker.name.lastName(),
        email: Faker.internet.email(),
      }).then(person => {
        return person.createPost({
          title: `Post by ${person.firstName}`,
          content: Faker.lorem.paragraph(),
        })
      })
    })
  })

export {
  Person,
  Post,
}

export default Conn